# A Formal Model to Study Safety Control Properties in an Adversarial Setting

This repo contains the software and additional content (proofs) that were summarized from the paper. 


## Model - A Matlab simulation

The software is a Simulink code that models SWaT as a Hybrid System. The `initialization.m` script sets initial values for all sensors and actuators included in the simulation. Variables `T` and `J` set the simulation horizon for continuous and discrete variables, respectively.

After running the `SWaT_3tanks.slx` model, the variables x11, x21, and x31 store the resulting traces of tanks T1, T2, and T3.

## Complete mathematical proofs

The _Model and Proofs of Security for SWaT_ document includes the detailed testbed model with theorems and proofs.
